# Welcome

SIBA is a SImple BAckup software wrote in Java 8 using Eclipse Mars. CLI ang GUI (Swing) version are available.

SIBA transforms a source directory to a tar.gz file with a tar.gz.md5 file.

# LICENSE

SIBA is released under the GNU AGPL license. Enjoy!

# Developer install

TODO

## Unit test environment
For unit tests, install the TestNG: 
* https://marketplace.eclipse.org/content/testng-eclipse
* Eclipse menu > Help > Eclipse Marketplace > Find "TestNG" > TestNG for Eclipse: Install button

# LOGO
SibaLogo comes from Cat-Box-32x32.png
http://www.iconspedia.com/icon/cat-box-icon-47051.html

Author: Iconka, http://iconka.com/
License: CC Attribution

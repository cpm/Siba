/*
 * Copyright (C) 2016 Didier Clermonté <dclermonte@april.org>
 * Copyright (C) 2016 Christian Pierre Momon <christian.momon@devinsy.fr>
 *
 * This file is part of Siba.
 *
 * Siba is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dclermonte.siba.model;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.dclermonte.siba.SibaException;

/**
 *
 * @author Didier Clermonté (dclermonte@april.org)
 *
 */
public final class SibaManager
{
    private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("org.dclermonte.siba.model.messages");

    private SibaManager()
    {

    }

    /**
     * This method perform the backup.
     *
     * @param fileToSave
     *            name of the directory to save
     * @return the archive of the file
     * @throws SibaException
     *             specific exception for Siba
     */
    public static File backup(final File fileToSave) throws SibaException
    {
        File result;

        File target = new File(System.getProperty("user.dir"));
        result = backup(fileToSave, target);

        //
        return result;
    }

    /**
     * This method performs the backup.
     *
     * @param fileToSave
     *            name of the directory to save
     * @param target
     *            directory for the file
     * @return the archive of the file
     * @throws SibaException
     *             specific exception
     */
    public static File backup(final File fileToSave, final File target) throws SibaException
    {
        File result;

        TarArchiveOutputStream out = null;
        try
        {
            if ((fileToSave == null) || !fileToSave.exists())
            {
                throw new SibaException(BUNDLE.getString("sourceNotExist.text"));
            }
            else if (!fileToSave.isDirectory())
            {
                throw new SibaException(BUNDLE.getString("NotDirectorySource.text"));
            }
            else if (!target.isDirectory())
            {
                throw new SibaException(BUNDLE.getString("NotDirectoryTarget.text"));
            }
            else
            {
                LocalDateTime date = LocalDateTime.now();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH'h'mm'mn'ss's'");
                String textDate = date.format(formatter);
                String outputFileNameWithoutExtension = fileToSave.getName() + "-" + textDate;
                result = new File(target, outputFileNameWithoutExtension + ".tgz");
                OutputStream gzipOutputStream = new GzipCompressorOutputStream(
                        new BufferedOutputStream(new FileOutputStream(result)));
                out = new TarArchiveOutputStream(gzipOutputStream);
                SibaUtils.tarDirectoryTree(fileToSave, out);
                out.close();
                SibaUtils.createMD5File(result, target + File.separator + outputFileNameWithoutExtension + ".tgz.md5");
            }
        }
        catch (IOException exception)
        {
            throw new SibaException(BUNDLE.getString("ioExceptionBackup.text"), exception);
        }
        catch (NoSuchAlgorithmException exception)
        {
            throw new SibaException(BUNDLE.getString("noSuchAlgorithm.text"), exception);
        }
        finally
        {
            IOUtils.closeQuietly(out);
        }

        //
        return result;
    }

    /**
     * This method is used to check the file.
     *
     * @param md5FileToCheck
     *            the file to check
     * @return the result of check
     * @throws SibaException
     *             specific exception
     */
    public static boolean check(final File md5FileToCheck) throws SibaException
    {
        boolean result;

        try
        {
            String md5String = SibaUtils.loadMD5Sum(md5FileToCheck);
            File fileToCheck1 = SibaUtils.readFileNameToCheck(md5FileToCheck);
            if (StringUtils.equals(md5String, SibaUtils.md5(fileToCheck1)))
            {
                result = true;
            }
            else
            {
                result = false;
            }
        }
        catch (IOException exception)
        {
            throw new SibaException(BUNDLE.getString("ioExceptionCheck.text"), exception);
        }
        catch (NoSuchAlgorithmException exception)
        {
            throw new SibaException(BUNDLE.getString("noSuchAlgorithm.text"), exception);
        }

        //
        return result;
    }

}

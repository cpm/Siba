/*
 * Copyright (C) 2016 Didier Clermonté <dclermonte@april.org>
 * Copyright (C) 2016 Christian Pierre Momon <christian.momon@devinsy.fr>
 *
 * This file is part of Siba.
 *
 * Siba is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dclermonte.siba.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author Didier Clermonté (dclermonte@april.org)
 *
 */
public final class SibaUtils
{
    public static final String DEFAULT_CHARSET = "UTF-8";

    /**
     *
     */
    private SibaUtils()
    {
    }

    /**
     *
     * This method Generate the file with MD5.
     *
     * @param inputFile
     *            the file on which the md5 is calculate
     * @param target
     *            the MD5 file
     * @return the MD5 file
     * @throws IOException
     *             IOException
     * @throws NoSuchAlgorithmException
     *             NoSuchAlgorithmException
     */
    public static File createMD5File(final File inputFile, final String target)
            throws IOException, NoSuchAlgorithmException
    {
        File result;

        String md5 = SibaUtils.md5(inputFile);
        String fileName = inputFile.getName();
        result = createMD5File(md5, fileName, target);

        //
        return result;
    }

    /**
     *
     * @param md5
     *            Hash MD5
     * @param fileName
     *            The name of the file
     * @param target
     *            directory to put the file
     * @return The MD5 file
     * @throws IOException
     *             IOexception
     */
    public static File createMD5File(final String md5, final String fileName, final String target) throws IOException
    {
        File result;

        FileWriter out = null;
        try
        {
            result = new File(target);
            out = new FileWriter(result);
            out.write(md5);
            String newLine = System.getProperty("line.separator");
            out.append(" ");
            out.append(fileName);
            out.append(newLine);
            out.close();
        }
        finally
        {
            IOUtils.closeQuietly(out);
        }

        //
        return result;
    }

    /**
     *
     * @param md5File
     *            the file to check
     * @return The MD5sum
     * @throws IOException
     *             IOException
     */
    public static String loadMD5Sum(final File md5File) throws IOException
    {
        String result;

        FileReader in = null;
        try
        {
            if (md5File.exists() && (md5File.length() > 32))
            {
                in = new FileReader(md5File);
                BufferedReader bufferedReader = new BufferedReader(in);
                String line = bufferedReader.readLine();
                result = line.substring(0, 32);
                in.close();
            }
            else
            {
                result = "";
            }
        }
        finally
        {
            IOUtils.closeQuietly(in);
        }

        //
        return result;
    }

    /**
     * This method calculate the MD5 itself.
     *
     * @param input
     *            The name of the file for witch the MD5 sum is to calculate
     * @return The MD5Sum
     * @throws IOException
     *             IOException
     * @throws NoSuchAlgorithmException
     *             NoSuchAlgorithmException
     */
    public static String md5(final File input) throws IOException, NoSuchAlgorithmException
    {
        String result;

        StringBuilder hashString = new StringBuilder();
        FileInputStream fileInputStream = null;
        try
        {
            byte[] byteInput = new byte[(int) input.length()];
            fileInputStream = new FileInputStream(input);
            for (int index1 = 0; index1 < input.length(); index1++)
            {
                byteInput[index1] = (byte) fileInputStream.read();
            }
            byte[] hash = null;
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            hash = messageDigest.digest(byteInput);

            for (int index = 0; index < hash.length; index++)
            {
                String hex = Integer.toHexString(hash[index]);
                if (hex.length() == 1)
                {
                    hashString.append('0');
                    hashString.append(hex.charAt(hex.length() - 1));
                }
                else
                {
                    hashString.append(hex.substring(hex.length() - 2));
                }
            }
            fileInputStream.close();

            //
            result = hashString.toString();
        }
        finally
        {
            IOUtils.closeQuietly(fileInputStream);
        }

        //
        return result;
    }

    /**
     *
     * @param md5File
     *            The MD5file
     * @return the file to check
     * @throws IOException
     *             IOException
     */
    public static File readFileNameToCheck(final File md5File) throws IOException
    {
        File result;

        FileReader in = null;
        try
        {
            if (md5File.exists() && (md5File.length() > 32))
            {
                in = new FileReader(md5File);
                BufferedReader bufferedReader = new BufferedReader(in);
                String line = bufferedReader.readLine();
                String fileNameToString;
                fileNameToString = line.substring(33);
                in.close();
                if (md5File.isAbsolute())
                {
                    result = new File(md5File.getParent(), fileNameToString);
                }
                else
                {
                    String path = System.getProperty("user.dir");
                    result = new File(path, fileNameToString);
                }
            }
            else
            {
                result = null;
            }
        }
        finally
        {
            IOUtils.closeQuietly(in);
        }

        //
        return result;
    }

    /**
     *
     * @param resource
     *            the resource to read
     * @return the string of the resource
     * @throws IOException
     *             IOException
     */
    public static String readResourceFromJar(final String resource) throws IOException
    {
        String result;

        URL url = SibaUtils.class.getResource(resource);
        result = IOUtils.toString(url, DEFAULT_CHARSET);

        //
        return result;
    }

    /**
     *
     * @param fileToSave
     *            The directory to backup
     * @param outputStream
     *            local Stream
     * @throws IOException
     *             IOException
     */
    public static void tarDirectoryTree(final File fileToSave, final TarArchiveOutputStream outputStream)
            throws IOException
    {
        int pathLength = fileToSave.getParentFile().getAbsolutePath().length();
        tarDirectoryTree(fileToSave, outputStream, pathLength);
    }

    /**
     *
     * This method generates ArchiveEntry.
     *
     * @param directory
     *            The actual directory being archived
     * @param outputStream
     *            local stream
     * @param pathLength
     *            used to select the starting directory
     * @throws IOException
     *             IOException
     */
    public static void tarDirectoryTree(final File directory, final TarArchiveOutputStream outputStream,
            final int pathLength) throws IOException
    {
        for (File file : directory.listFiles())
        {
            if (file.isDirectory())
            {
                if (file.listFiles().length == 0)
                {
                    TarArchiveEntry tarArchiveEntry = new TarArchiveEntry(file);
                    String pathPartiel = file.getAbsolutePath().substring(pathLength);
                    tarArchiveEntry.setName(pathPartiel);
                    outputStream.putArchiveEntry(tarArchiveEntry);
                    outputStream.closeArchiveEntry();
                }
                else
                {
                    tarDirectoryTree(file, outputStream, pathLength);
                }
            }
            else
            {
                TarArchiveEntry tarArchiveEntry = new TarArchiveEntry(file);
                String pathPartiel = file.getAbsolutePath().substring(pathLength);
                tarArchiveEntry.setName(pathPartiel);
                outputStream.putArchiveEntry(tarArchiveEntry);
                IOUtils.copy(new FileInputStream(file), outputStream);
                outputStream.closeArchiveEntry();

            }
        }
    }
}

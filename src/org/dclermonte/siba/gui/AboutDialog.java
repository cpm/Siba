/*
 * Copyright (C) 2016 Didier Clermonté <dclermonte@april.org>
 * Copyright (C) 2016 Christian Pierre Momon <christian.momon@devinsy.fr>
 *
 * This file is part of Siba.
 *
 * Siba is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dclermonte.siba.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Locale;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;

import org.dclermonte.siba.model.SibaUtils;

/**
 *
 * @author dclermonte
 *
 */
public class AboutDialog extends JDialog
{
    private static final long serialVersionUID = 8868109575782482455L;

    /**
     * This is the constructor for the dialog.
     *
     * @throws IOException
     *             IOException
     */
    public AboutDialog() throws IOException
    {
        setMinimumSize(new Dimension(600, 350));
        setMaximumSize(new Dimension(600, 500));
        setIconImage(Toolkit.getDefaultToolkit()
                .getImage(AboutDialog.class.getResource("/org/dclermonte/siba/gui/SibaLogo.png")));
        setBackground(Color.GREEN);
        getContentPane().setBackground(Color.GREEN);
        setBounds(100, 100, 450, 300);
        getContentPane().setLayout(new BorderLayout());
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setBackground(Color.GREEN);
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                JButton okButton = new JButton("OK");
                okButton.setFocusTraversalKeysEnabled(false);
                okButton.setFocusPainted(false);
                okButton.setBackground(new Color(0, 128, 0));
                okButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                okButton.addActionListener(new ActionListener()
                {
                    @Override
                    public void actionPerformed(final ActionEvent e)
                    {
                        setVisible(false);
                        dispose();
                    }
                });
                buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.X_AXIS));
                {
                    Component horizontalGlue = Box.createHorizontalGlue();
                    buttonPane.add(horizontalGlue);
                }
                okButton.setActionCommand("OK");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
            }
            {
                Component horizontalGlue = Box.createHorizontalGlue();
                buttonPane.add(horizontalGlue);
            }
        }
        JPanel panel = new JPanel();
        panel.setBackground(Color.GREEN);
        getContentPane().add(panel, BorderLayout.CENTER);
        panel.setLayout(new BorderLayout(0, 0));

        JTextPane txtpnSimpleBackupDveloppeur = new JTextPane();
        txtpnSimpleBackupDveloppeur.setBackground(Color.GREEN);
        txtpnSimpleBackupDveloppeur.setFont(new Font("Dialog", Font.BOLD, 14));
        txtpnSimpleBackupDveloppeur.setContentType("text/html");
        String resource;
        String locale = Locale.getDefault().toString();
        if (locale.equals("fr_FR"))
        {
            resource = "/org/dclermonte/siba/gui/about_fr.html";
        }
        else
        {
            resource = "/org/dclermonte/siba/gui/about_en.html";
        }
        txtpnSimpleBackupDveloppeur.setText(SibaUtils.readResourceFromJar(resource));
        panel.add(txtpnSimpleBackupDveloppeur);
        {
            JLabel lblNewLabel = new JLabel("");
            lblNewLabel
                    .setIcon(new ImageIcon(AboutDialog.class.getResource("/org/dclermonte/siba/gui/SibaLogo_256.png")));
            panel.add(lblNewLabel, BorderLayout.WEST);
        }

    }
}

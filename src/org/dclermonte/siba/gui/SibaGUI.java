/*
 * Copyright (C) 2016 Didier Clermonté <dclermonte@april.org>
 * Copyright (C) 2016 Christian Pierre Momon <christian.momon@devinsy.fr>
 *
 * This file is part of Siba.
 *
 * Siba is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.dclermonte.siba.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.apache.commons.lang3.StringUtils;
import org.dclermonte.siba.SibaException;
import org.dclermonte.siba.gui.utils.GUIToolBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The main class for GUI (Graphical User Interface )
 *
 * @author dclermonte
 *
 */
public final class SibaGUI
{
    private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("org.dclermonte.siba.gui.messages");
    private static Logger logger = LoggerFactory.getLogger(SibaGUI.class);
    private JFrame frmSimpleBackup;

    /**
     * Launch the application.
     */

    private BackupPanel backupPanel;
    private CheckPanel checkPanel;

    /**
     * This is the constructor of this application.
     *
     * @throws UnsupportedLookAndFeelException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws ClassNotFoundException
     */
    private SibaGUI() throws ClassNotFoundException, InstantiationException, IllegalAccessException,
            UnsupportedLookAndFeelException, SibaException
    {
        final Locale locale;
        // Set default GUI catch.
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler()
        {
            @Override
            public void uncaughtException(final Thread thread, final Throwable exception)
            {
                String message;
                if (exception instanceof OutOfMemoryError)
                {
                    message = "Java ran out of memory!\n\n";
                }
                else
                {
                    message = String.format("An error occured: %1s(%2s)", exception.getClass(), exception.getMessage());
                }

                JOptionPane.showMessageDialog(SibaGUI.this.frmSimpleBackup, message, "Error",
                        JOptionPane.ERROR_MESSAGE);
                logger.error("uncaughtException ", exception);
            }
        });

        // Remove BOLD on default font.
        UIManager.put("swing.boldMetal", Boolean.FALSE);

        // Set LookAndFeel.
        logger.debug("System lookAndFeel property: {}", System.getProperty("swing.defaultlaf"));
        logger.debug("Available lookAndFeel: {} ", GUIToolBox.availableLookAndFeels().toString());
        logger.debug("System lookAndFeel: {} ", UIManager.getSystemLookAndFeelClassName());
        logger.debug("Current lookAndFeel: {} ", UIManager.getLookAndFeel().getName());

        if (!StringUtils.equals(UIManager.getSystemLookAndFeelClassName(), "javax.swing.plaf.metal.MetalLookAndFeel"))
        {
            try
            {
                logger.debug("Metal LAF setted and system LAF detected, try to set system LAF.");
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            }
            catch (final Exception exception)
            {
                logger.debug("Failed to set the system LookAndFeel.");
            }
        }
        else if (GUIToolBox.availableLookAndFeels().toString().contains("GTK+"))
        {
            try
            {
                logger.debug("Metal LAF setted and GTK+ LAF detected, try to set GTK+ LAF.");
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
            }
            catch (final Exception exception)
            {
                logger.debug("Failed to set the system LookAndFeel.");
            }
        }
        logger.debug("Activated lookAndFeel: {} ", UIManager.getLookAndFeel().getName());

        // Set default locale.
        locale = Locale.getDefault();
        updateLanguage(locale);
        logger.debug(locale.getDisplayLanguage());
        initialize();

    }

    /**
     * Initialize the contents of the frame.
     *
     * @throws SibaException
     */
    private void initialize() throws SibaException
    {
        this.frmSimpleBackup = new JFrame();
        this.frmSimpleBackup.setIconImage(Toolkit.getDefaultToolkit()
                .getImage(SibaGUI.class.getResource("/org/dclermonte/siba/gui/SibaLogo.png")));
        this.frmSimpleBackup.getContentPane().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        this.frmSimpleBackup.setMinimumSize(new Dimension(600, 400));

        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        tabbedPane.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        this.frmSimpleBackup.getContentPane().add(tabbedPane, BorderLayout.CENTER);

        this.backupPanel = new BackupPanel();
        tabbedPane.addTab(BUNDLE.getString("BackupPanel.TitleBackupPanel.text"),
                new ImageIcon(SibaGUI.class.getResource("/org/dclermonte/siba/gui/SibaLogo.png")), this.backupPanel,
                null);
        this.checkPanel = new CheckPanel();
        this.checkPanel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        tabbedPane.addTab(BUNDLE.getString("BackupPanel.TitleCheckPanel.text"),
                new ImageIcon(SibaGUI.class.getResource("/org/dclermonte/siba/gui/SibaLogo.png")), this.checkPanel,
                null);
        this.frmSimpleBackup.setFont(new Font("DejaVu Sans", Font.BOLD, 12));
        this.frmSimpleBackup.setForeground(new Color(0, 100, 0));
        this.frmSimpleBackup.setBackground(new Color(102, 205, 170));
        this.frmSimpleBackup.setTitle("Simple Backup");
        this.frmSimpleBackup.setBounds(100, 100, 450, 300);
        this.frmSimpleBackup.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JMenuBar menuBar = new JMenuBar();
        menuBar.setBackground(Color.GREEN);
        this.frmSimpleBackup.setJMenuBar(menuBar);

        JMenu mnFile = new JMenu(BUNDLE.getString("SibaGUI.mnFile.text"));
        mnFile.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        mnFile.setMnemonic('F');
        mnFile.setForeground(Color.BLACK);
        menuBar.add(mnFile);

        JMenuItem mntmBackup = new JMenuItem(BUNDLE.getString("SibaGUI.mntmBackup.text"));
        mntmBackup.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        mntmBackup.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(final ActionEvent e)
            {
                tabbedPane.setSelectedIndex(0);
                File file = SourceDirectorySelector.showSelectorDialog(SibaGUI.this.backupPanel, null);
                if (file != null)
                {
                    SibaGUI.this.backupPanel.setSourceDirectory(file.getPath());
                }
            }
        });
        mnFile.add(mntmBackup);

        JMenuItem mntmCheck = new JMenuItem(BUNDLE.getString("SibaGUI.mntmCheck.text"));
        mntmCheck.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        mntmCheck.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(final ActionEvent e)
            {
                tabbedPane.setSelectedIndex(1);
                File choosenFile = CheckDirectorySelector.showSelectorDialog(SibaGUI.this.checkPanel, null);
                if ((choosenFile != null) && choosenFile.exists())
                {
                    SibaGUI.this.checkPanel.setFileToCheck(choosenFile.getAbsolutePath());
                }
            }
        });
        mnFile.add(mntmCheck);

        JMenuItem mntmQuit = new JMenuItem(BUNDLE.getString("SibaGUI.mntmQuit.text"));
        mntmQuit.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        mntmQuit.setMnemonic(KeyEvent.VK_Q);
        mntmQuit.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(final ActionEvent arg0)
            {
                SibaGUI.this.frmSimpleBackup.dispose();
            }
        });

        JSeparator separator = new JSeparator();
        mnFile.add(separator);
        mnFile.add(mntmQuit);

        JMenu mnNewMenu = new JMenu(BUNDLE.getString("SibaGUI.mnNewMenu.text"));
        mnNewMenu.setBackground(Color.GREEN);
        mnNewMenu.setMnemonic('H');
        mnNewMenu.setLocation(new Point(500, 0));
        mnNewMenu.setHorizontalTextPosition(SwingConstants.RIGHT);
        mnNewMenu.setHorizontalAlignment(SwingConstants.RIGHT);
        mnNewMenu.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        mnNewMenu.setForeground(new Color(0, 0, 0));
        mnNewMenu.setOpaque(true);
        menuBar.add(mnNewMenu);

        JMenuItem mntmMenuItemAbout = new JMenuItem(BUNDLE.getString("SibaGUI.mntmNewMenuItem_2.text"));
        mntmMenuItemAbout.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        mntmMenuItemAbout.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(final ActionEvent e)
            {
                AboutDialog dialog;
                try
                {
                    dialog = new AboutDialog();
                    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
                    dialog.setVisible(true);
                }
                catch (IOException exception)
                {
                    logger.error("Exception ", exception);
                }
            }
        });
        mnNewMenu.add(mntmMenuItemAbout);
    }

    /**
     * This method launch the GUI.
     */
    public static void run()
    {
        EventQueue.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    SibaGUI window = new SibaGUI();
                    window.frmSimpleBackup.setVisible(true);
                }
                catch (SibaException exception)
                {
                    logger.error("SibaException ", exception);
                    String dataMissing = exception.getMessage();
                    String titleWarning = BUNDLE.getString("BackupPanel.TitleWarning.text");
                    JOptionPane.showMessageDialog(null, dataMissing, titleWarning, JOptionPane.INFORMATION_MESSAGE);
                }
                catch (Exception exception)
                {
                    logger.error("Exception ", exception);
                }
            }
        });
    }

    /**
     * This method set the local language.
     *
     * @param source
     *            locale
     */
    public static void updateLanguage(final Locale source)
    {
        // Change JVM default locale.
        java.util.Locale.setDefault(source);

        // Change LookAndFeel default locale.
        javax.swing.UIManager.getDefaults().setDefaultLocale(source);

        // Change new component default locale.
        javax.swing.JComponent.setDefaultLocale(source);

        //
        ResourceBundle.clearCache();
    }

}

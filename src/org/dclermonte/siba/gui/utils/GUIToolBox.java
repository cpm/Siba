/*
 * Copyright (C) 2016 Didier Clermonté <dclermonte@april.org>
 * Copyright (C) 2016 Christian Pierre Momon <christian.momon@devinsy.fr>
 *
 * This file is part of Siba.
 *
 * Siba is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dclermonte.siba.gui.utils;

import java.util.ArrayList;
import java.util.List;

import javax.swing.UIManager;

/**
 * Some utility
 *
 * @author dclermonte
 *
 */
public final class GUIToolBox
{
    /**
     *
     */
    private GUIToolBox()
    {
    }

    /**
     *
     * @return string with lookAndFeel
     */
    public static List<String> availableLookAndFeels()
    {
        List<String> result;

        //
        result = new ArrayList<String>();

        //
        for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())
        {
            //
            result.add(info.getName());
        }

        //
        return result;
    }

}
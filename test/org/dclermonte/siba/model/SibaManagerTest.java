package org.dclermonte.siba.model;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.io.FileUtils;
import org.dclermonte.siba.SibaException;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 *
 * @author cpm
 */
public class SibaManagerTest
{
    /**
     *
     */
    @Test
    public void aFooTest()
    {
        Assert.assertTrue(true);
    }

    /**
     *
     * @throws IOException
     *             IOException
     * @throws ArchiveException
     *             ArchiveException
     * @throws SibaException
     *             specific sibaException
     */
    @Test(expectedExceptions = SibaException.class)
    public void backupTest01() throws ArchiveException, IOException, SibaException
    {
        SibaManager.backup(null, null);
    }

    /**
     * Test the archive generated
     *
     * @throws ArchiveException
     *             ArchiveException
     * @throws IOException
     *             IOException
     * @throws SibaException
     *             specific sibaException
     */
    @Test
    public void backupTest02() throws ArchiveException, IOException, SibaException
    {
        File source = new File("test/org/dclermonte/siba/data/simplestuff");

        File target = SibaManager.backup(source, new File(System.getProperty("java.io.tmpdir")));
        File reference = new File("test/org/dclermonte/siba/data/reference/simplestuff-2016-08-30T22h44mn21s.tgz");

        Boolean fileEquals = FileUtils.contentEquals(reference, target);

        File targetMD5 = new File(target.getAbsolutePath() + ".md5");
        target.delete();
        targetMD5.delete();
        // Test.
        Assert.assertTrue(fileEquals);
    }

    /**
     * Test the md5 calculation
     *
     * @throws ArchiveException
     *             Archive Exception
     * @throws IOException
     *             IOException
     * @throws SibaException
     *             specific sibaException
     * @throws NoSuchAlgorithmException
     *             NosuchAlgorithmException
     */
    @Test
    public void backupTest03() throws ArchiveException, IOException, SibaException, NoSuchAlgorithmException
    {
        File source = new File("test/org/dclermonte/siba/data/reference/simplestuff-2016-08-30T22h44mn21s.tgz");

        String md5Calculate = SibaUtils.md5(source);

        File reference = new File("test/org/dclermonte/siba/data/reference/simplestuff-2016-08-30T22h44mn21s.tgz.md5");
        String md5Reference = SibaUtils.loadMD5Sum(reference);

        // Test.
        Assert.assertEquals(md5Calculate, md5Reference);

    }
}
